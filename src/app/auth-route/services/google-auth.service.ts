import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';
import { GoogleAuthUser } from '../models/google-auth-user.model';
import { UserService } from 'src/app/common/user/services/user-service.service';
import { RouteUrlRecordService } from 'src/app/auth-route/services/route-url-record.service';
import { Preferences } from '@capacitor/preferences';

import {
  SignInWithApple,
  SignInWithAppleResponse,
  SignInWithAppleOptions,
} from '@capacitor-community/apple-sign-in';

@Injectable({
  providedIn: 'root'
})
export class GoogleAuthService {

  private isLoginIn = false;
  private currentUser!: GoogleAuthUser;

  constructor(
    private routeUrlRecordService: RouteUrlRecordService<{}>,
    private router: Router,
    private userService: UserService,
  ) { 
    // 於此取得初始資料
    GoogleAuth.initialize({
      clientId: '328066296243-5opekodfa93rria1e8utcql4rkrbvktq.apps.googleusercontent.com',
      scopes: ['profile', 'email'],
      grantOfflineAccess: true,
    });
    this.checkToken();
  }


  isAuthenticated() {
    return this.isLoginIn;
  }

  getCurrentUser() {
    return this.currentUser;
  }

  /*
   * 登出 
   */
  logout() {
    GoogleAuth.signOut();
    this.currentUser = new GoogleAuthUser({});
    this.isLoginIn = false;
    this.cleanToken();
    this.router.navigate(["/"]);
  }

  /*
   * google 第三方登入 
   */
  async loginByGoogle() {
    // 登入判斷
    const user = await GoogleAuth.signIn();
    if(user["email"]) {
      this.userService
        .getUserIdByMail(user["email"])
        .then(doc => {
          if(!doc.docs[0]) {
            this.registerUserForFirstLogin(doc, user);
          } else {
            this.setCurrentUser(doc, user);
            this.setToken();
            this.checkLogin(user);
          }
        });
    }

    // 等待超過 7秒, 使用測試帳號登入
    setTimeout(()=> {
      if(!this.currentUser) {
        this.getTestUserInfo();
      }
    }, 7000);
  }

  /* 
   * apple 第三方登入
   */
  async loginByApple() {
    let options: SignInWithAppleOptions = {
      clientId: 'com.spacein.space-in',
      redirectURI: 'https://spacein-ec8ab.firebaseapp.com/__/auth/handler',
      scopes: 'email name',
      state: '12345',
      nonce: 'nonce',
    };

    SignInWithApple.authorize(options)
      .then((result: SignInWithAppleResponse) => {
        // Handle user information
        // Validate token with server and create new session
        alert(" apple 登入 result : " + result);
      })
      .catch(error => {
        // Handle error
        alert(error.toString());
        console.log(error)
      });
  }

  /*
   * 且首次登入, 註冊進 firestore user 表, 且 google auth 成功,
   * 依照 email 新增一筆
   */
  registerUserForFirstLogin(doc: any, user: any) {
    this.userService
      //.addUser(new GoogleAuthUser(user))
      .addUser(user)
      .then(() => {
        alert("首次 google 授權, 註冊成功, 已可使用google帳號直接登入");
        //this.setCurrentUser(doc, user);
        this.router.navigate(["/"]);
      });
  }

  /*
   * 取得 userFirestoreId 並設定 current user
   */
  setCurrentUser(doc:any, user:any) {
    const userFirestoreId = doc.docs[0]._key.path.segments[6];
    const newUser = {
      userFirestoreId: userFirestoreId,
      ...user,
    }
    this.currentUser = new GoogleAuthUser(newUser);
  }

  /* 
   * 成功登入後, 將 token 資訊 使用 csp存入手機端
   */
  async setToken() {
    await Preferences.set({key: "currentUser", value: JSON.stringify(this.currentUser)});
    await Preferences.set({key: "token", value: JSON.stringify(new Date().getTime())});
  }

  /*
   *  下次開啟 app, 檢查 token, 若在期限內便自動登入
   */
  async checkToken() {
    const _currentUser = await Preferences.get({key: "currentUser"});
    const _token = await Preferences.get({key: "token"}) || 0;
    if(!_currentUser.value) {
      return;
    }
    if(!_token.value) {
      return;
    }
    const loginDateTimeSpan = Number.parseInt(_token.value?_token.value:"0");
    const currentTimeSpan = new Date().getTime();
    if((currentTimeSpan - loginDateTimeSpan)/3600000 <= 720) {
      this.currentUser = new GoogleAuthUser(JSON.parse(!_currentUser.value?"":_currentUser.value));
      this.checkLogin(this.currentUser);
    }
    else {
      alert("閒置過久, 請重新登入");
      this.logout();
    }
  }

  /*
   * 清空 token 
   */
  async cleanToken() {
    await Preferences.remove({key: "currentUser"});
    await Preferences.remove({key: "token"});
  }
  
  /*
   * 檢查是否成功登入 
   */
  checkLogin(user: any) {
    if (this.currentUser) {
      alert(user.email + " 成功登入!");
      this.isLoginIn = true;
      this.routeUrlRecordService.nextPage("home", {});
    }
    else {
      alert("無法取得使用者資訊");
    }
  }

  /* 
   * 測試 帳密登入, 供給 apple 測試人員使用
   */
  loginTestAccount(account:string, password:string) {
    if(account === "testuser" && password === "123456") {
      this.getTestUserInfo();
    } else {
      alert("帳密錯誤");
    }
  }

  /*
   * 取得測試登入帳號 
   */
  getTestUserInfo() {
    this.currentUser = new GoogleAuthUser({
      id: "testid",
      name: "testname",
      email: "tesetmail@testmail.com",
      imageUrl: "testurl",
      authentication: {},
    });
    alert("測試帳號: " + this.currentUser.email);
    this.isLoginIn = true;
    this.routeUrlRecordService.nextPage("home", {});
  }

  /* 
   * 刪除使用者帳號
   * 配合 ios 上架規範
   */
  deleteUserById(id: string) {
    if(!confirm("再次確認刪除帳號? 刪除後該帳號所有預約記錄將不會保留!")) {
      return;
    }
    if(!id) {
      alert("測試帳號無法刪除 : " + this.currentUser.email);
      return;
    }
    this.userService.deleteUserById(id);
    this.routeUrlRecordService.nextPage("/", {}); //登出, 進入登入頁
  }

}
