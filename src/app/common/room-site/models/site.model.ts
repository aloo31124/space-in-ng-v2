
export class Site {
    // 座位id
    fireStoreId = "";
    // 座位名稱
    name = "";
    // 座位 所屬之 教室id
    roomId = "";

    constructor(site: any) {
        if(!site) {
            return;
        }
        this.fireStoreId = site["fireStoreId"];
        this.name = site["name"];
        this.roomId = site["roomId"];
    }
    
}
